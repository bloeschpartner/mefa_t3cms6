<?php
return array(
	'BE' => array(
		'disable_exec_function' => '0',
		'fileCreateMask' => '0644',
		'folderCreateMask' => '0755',
		'installToolPassword' => '65b787d2c0837b674c4044b48ac2bc4d',
		'loginSecurityLevel' => 'rsa',
		'maxFileSize' => '20480',
		'versionNumberInFilename' => '0',
	),
	'DB' => array(
		'database' => 'mefa_oben_old',
		'extTablesDefinitionScript' => 'extTables.php',
		'host' => 'localhost',
		'password' => 'root',
		'username' => 'root',
	),
	'EXT' => array(
		'extConf' => array(
			'em' => 'a:1:{s:17:"selectedLanguages";s:2:"de";}',
			'languagevisibility' => 'a:6:{s:18:"inheritanceEnabled";s:1:"0";s:26:"translatedAsDefaultEnabled";s:1:"0";s:8:"useCache";s:1:"0";s:24:"observeCutCopyMoveAction";s:1:"1";s:30:"restrictCutCopyMoveForOverlays";s:1:"0";s:12:"applyPatchTV";s:1:"0";}',
			'naw_securedl' => 'a:15:{s:11:"securedDirs";s:16:"fileadmin/secure";s:8:"filetype";s:58:"pdf|jpe?g|gif|png|odt|ppt?x|doc?x|xls?x|zip|rar|tgz|tar|gz";s:13:"forcedownload";s:1:"1";s:17:"forcedownloadtype";s:44:"odt|ppt?x|doc?x|xls?x|zip|rar|tgz|tar|gz|pdf";s:19:"additionalMimeTypes";s:29:"txt|text/plain,html|text/html";s:6:"domain";s:32:"http://mefa.de/fileadmin/secure/";s:12:"cachetimeadd";s:3:"180";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:9:"524288000";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}',
			'realurl' => 'a:5:{s:10:"configFile";s:26:"typo3conf/realurl_conf.php";s:14:"enableAutoConf";s:1:"1";s:14:"autoConfFormat";s:1:"1";s:12:"enableDevLog";s:1:"0";s:19:"enableChashUrlDebug";s:1:"0";}',
			'saltedpasswords' => 'a:2:{s:3:"FE.";a:2:{s:7:"enabled";s:1:"1";s:21:"saltedPWHashingMethod";s:28:"tx_saltedpasswords_salts_md5";}s:3:"BE.";a:2:{s:7:"enabled";s:1:"1";s:21:"saltedPWHashingMethod";s:28:"tx_saltedpasswords_salts_md5";}}',
			'simulatestatic' => 'a:0:{}',
			'templavoila' => 'a:3:{s:7:"enable.";a:3:{s:13:"oldPageModule";s:1:"0";s:19:"selectDataStructure";s:1:"0";s:15:"renderFCEHeader";s:1:"0";}s:9:"staticDS.";a:3:{s:6:"enable";s:1:"1";s:8:"path_fce";s:27:"fileadmin/templates/ds/fce/";s:9:"path_page";s:28:"fileadmin/templates/ds/page/";}s:13:"updateMessage";s:1:"0";}',
		),
		'extListArray' => array(
			'css_styled_content',
			'extbase',
			'info',
			'perm',
			'func',
			'filelist',
			'about',
			'version',
			'tsconfig_help',
			'context_help',
			'extra_page_cm_options',
			'impexp',
			'sys_note',
			'tstemplate',
			'tstemplate_ceditor',
			'tstemplate_info',
			'tstemplate_objbrowser',
			'tstemplate_analyzer',
			'func_wizards',
			'wizard_crpages',
			'wizard_sortpages',
			'lowlevel',
			'install',
			'belog',
			'beuser',
			'aboutmodules',
			'setup',
			'taskcenter',
			'info_pagetsconfig',
			'viewpage',
			'rtehtmlarea',
			't3skin',
			't3editor',
			'reports',
			'felogin',
			'form',
			'fluid',
			'cshmanual',
			'feedit',
			'opendocs',
			'recycler',
			'scheduler',
			'workspaces',
			'rsaauth',
			'saltedpasswords',
			'static_info_tables',
			'templavoila',
			'realurl',
			'perfectlightbox',
			'naw_securedl',
			'simulatestatic',
		),
		'extList_FE' => 'css_styled_content,extbase,version,install,rtehtmlarea,t3skin,felogin,form,fluid,feedit,workspaces,rsaauth,saltedpasswords,static_info_tables,templavoila,realurl,perfectlightbox,naw_securedl',
		'noEdit' => '0',
	),
	'FE' => array(
		'loginSecurityLevel' => 'rsa',
	),
	'GFX' => array(
		'TTFdpi' => '96',
		'gdlib_png' => '1',
		'im_noScaleUp' => '1',
		'im_path' => '/usr/local/bin/',
		'im_path_lzw' => '/usr/local/bin/',
		'im_stripProfileCommand' => '',
		'im_version_5' => 'gm',
		'jpg_quality' => '90',
		'thumbnails_png' => '1',
	),
	'INSTALL' => array(
		'wizardDone' => array(
			'TYPO3\CMS\Install\CoreUpdates\InstallSysExtsUpdate' => '["info","perm","func","filelist","about","cshmanual","feedit","opendocs","recycler","t3editor","reports","scheduler","simulatestatic"]',
			'TYPO3\CMS\Install\Updates\TceformsUpdateWizard' => 'tt_content:image,pages:media,pages_language_overlay:media',
		),
	),
	'SYS' => array(
		'compat_version' => '6.1',
		'cookieHttpOnly' => 0,
		'cookieSecure' => 0,
		'doNotCheckReferer' => '1',
		'enableDeprecationLog' => 0,
		'encryptionKey' => 'PYckowdBZA4rw+bNe2p843g/VMkUNDp/5KPqsQyrj64Xev3dg0o/4+A+62EoH5dHUKEM9xdy4GiHynrKl4MWU2iqy+qYS9KP',
		'sitename' => 'MEFA Befestigungs- und Montagesysteme GmbH',
		't3lib_cs_convMethod' => 'mbstring',
		't3lib_cs_utils' => 'mbstring',
	),
);
?>