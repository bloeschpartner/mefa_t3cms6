<?php
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']=array (
  'mefa.de' => 
  array (
    'init' => 
    array (
      'enableCHashCache' => true,
      'appendMissingSlash' => 'ifNotFile,redirect',
      'adminJumpToBackend' => true,
      'enableUrlDecodeCache' => true,
      'enableUrlEncodeCache' => true,
      'emptyUrlReturnValue' => '/',
      'respectSimulateStaticURLs' => true,
    ),
    'pagePath' => 
    array (
      'type' => 'user',
      'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
      'spaceCharacter' => '-',
      'languageGetVar' => 'L',
      'rootpage_id' => '1',
    ),
    'fileName' => 
    array (
      'defaultToHTMLsuffixOnPrev' => 0,
      'acceptHTMLsuffix' => 1,
    ),
    'preVars' => 
    array (
      0 => 
      array (
        'GETvar' => 'L',
        'valueMap' => 
        array (
          'en' => '1',
        ),
        'noMatch' => 'bypass',
      ),
    ),
  ),
  'www.mefa.de' => 'mefa.de',
  'mefa-energy.de' => 'mefa.de',
  'www.mefa-energy.de' => 'mefa.de',
  'mefa-energy-systems.de' => 'mefa.de',
  'www.mefa-energy-systems.de' => 'mefa.de',
  'mefa-rohrmontagesysteme.de' => 'mefa.de',
  'www.mefa-rohrmontagesysteme.de' => 'mefa.de',
  'mefaenergysystems.de' => 'mefa.de',
  'www.mefaenergysystems.de' => 'mefa.de',
  'oben-sein.com' => 'mefa.de',
  'www.oben-sein.com' => 'mefa.de',
  'oben-sein.de' => 'mefa.de',
  'www.oben-sein.de' => 'mefa.de',
  'obensein.com' => 'mefa.de',
  'www.obensein.com' => 'mefa.de',
  'mefa.oben.old' => 
  array (
    'init' => 
    array (
      'enableCHashCache' => true,
      'appendMissingSlash' => 'ifNotFile,redirect',
      'adminJumpToBackend' => true,
      'enableUrlDecodeCache' => true,
      'enableUrlEncodeCache' => true,
      'emptyUrlReturnValue' => '/',
      'respectSimulateStaticURLs' => true,
    ),
    'pagePath' => 
    array (
      'type' => 'user',
      'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
      'spaceCharacter' => '-',
      'languageGetVar' => 'L',
      'rootpage_id' => '1',
    ),
    'fileName' => 
    array (
      'defaultToHTMLsuffixOnPrev' => 0,
      'acceptHTMLsuffix' => 1,
    ),
    'preVars' => 
    array (
      0 => 
      array (
        'GETvar' => 'L',
        'valueMap' => 
        array (
          'en' => '1',
        ),
        'noMatch' => 'bypass',
      ),
    ),
  ),
);
