<?php
$TYPO3_CONF_VARS['SYS']['sitename']            = 'p187182';
$TYPO3_CONF_VARS['SYS']['encryptionKey']       = 'PYckowdBZA4rw+bNe2p843g/VMkUNDp/5KPqsQyrj64Xev3dg0o/4+A+62EoH5dHUKEM9xdy4GiHynrKl4MWU2iqy+qYS9KP';

$TYPO3_CONF_VARS['BE']['installToolPassword']  = '031d60e55bb34437b700a6c95042ad5b';

$TYPO3_CONF_VARS['EXT']['extList'] = 'info,perm,func,filelist,about,version,tsconfig_help,context_help,extra_page_cm_options,impexp,sys_note,tstemplate,tstemplate_ceditor,tstemplate_info,tstemplate_objbrowser,tstemplate_analyzer,func_wizards,wizard_crpages,wizard_sortpages,lowlevel,install,belog,beuser,aboutmodules,setup,taskcenter,info_pagetsconfig,viewpage,rtehtmlarea,css_styled_content,t3skin,t3editor,reports,felogin,form,extbase,fluid,cshmanual,feedit,opendocs,simulatestatic,recycler,scheduler,version,workspaces,rsaauth,saltedpasswords';
$TYPO3_CONF_VARS['EXT']['extConf']['saltedpasswords'] = 'a:2:{s:3:"FE.";a:2:{s:7:"enabled";s:1:"1";s:21:"saltedPWHashingMethod";s:28:"tx_saltedpasswords_salts_md5";}s:3:"BE.";a:2:{s:7:"enabled";s:1:"1";s:21:"saltedPWHashingMethod";s:28:"tx_saltedpasswords_salts_md5";}}';

$TYPO3_CONF_VARS['GFX']["im_path"]                = '/usr/local/bin/';
$TYPO3_CONF_VARS['GFX']['im_version_5']           = 'gm';
$TYPO3_CONF_VARS['GFX']["im_path_lzw"]            = '/usr/local/bin/';
$TYPO3_CONF_VARS['GFX']['TTFdpi']                 = '96';
$TYPO3_CONF_VARS['GFX']['im_stripProfileCommand'] = '';

$TYPO3_CONF_VARS['SYS']['t3lib_cs_convMethod'] = 'mbstring';
$TYPO3_CONF_VARS['SYS']['t3lib_cs_utils']      = 'mbstring';
$TYPO3_CONF_VARS['SYS']['doNotCheckReferer']   = '1';

$TYPO3_CONF_VARS['BE']['fileCreateMask']       = '0644';
$TYPO3_CONF_VARS['BE']['folderCreateMask']     = '0755';
$TYPO3_CONF_VARS['BE']['maxFileSize']          = '20480';
$TYPO3_CONF_VARS['BE']['loginSecurityLevel']   = 'rsa';

$TYPO3_CONF_VARS['SYS']['cookieHttpOnly']      = 0;
$TYPO3_CONF_VARS['SYS']['cookieSecure']        = 0;
$TYPO3_CONF_VARS['SYS']['enableDeprecationLog']= 0;

$TYPO3_CONF_VARS['FE']['loginSecurityLevel']   = 'rsa';

$typo_db                                       = 'mefa_oben_old';
$typo_db_username                              = 'root';
$typo_db_password                              = 'root';
$typo_db_host                                  = 'localhost';
$typo_db_extTableDef_script                    = 'extTables.php';

## INSTALL SCRIPT EDIT POINT TOKEN - all lines after this points may be changed by the install script!

$TYPO3_CONF_VARS['EXT']['extList'] = 'css_styled_content,extbase,info,perm,func,filelist,about,version,tsconfig_help,context_help,extra_page_cm_options,impexp,sys_note,tstemplate,tstemplate_ceditor,tstemplate_info,tstemplate_objbrowser,tstemplate_analyzer,func_wizards,wizard_crpages,wizard_sortpages,lowlevel,install,belog,beuser,aboutmodules,setup,taskcenter,info_pagetsconfig,viewpage,rtehtmlarea,t3skin,t3editor,reports,felogin,form,fluid,cshmanual,feedit,opendocs,recycler,scheduler,workspaces,rsaauth,saltedpasswords,static_info_tables,templavoila,realurl,perfectlightbox,naw_securedl';	// Modified or inserted by TYPO3 Extension Manager. 
$TYPO3_CONF_VARS['EXT']['extList_FE'] = 'css_styled_content,extbase,version,install,rtehtmlarea,t3skin,felogin,form,fluid,feedit,workspaces,rsaauth,saltedpasswords,static_info_tables,templavoila,realurl,perfectlightbox,naw_securedl';	// Modified or inserted by TYPO3 Extension Manager. 
$TYPO3_CONF_VARS['EXT']['extConf']['templavoila'] = 'a:2:{s:7:"enable.";a:3:{s:13:"oldPageModule";s:1:"0";s:19:"selectDataStructure";s:1:"0";s:15:"renderFCEHeader";s:1:"0";}s:9:"staticDS.";a:3:{s:6:"enable";s:1:"0";s:8:"path_fce";s:27:"fileadmin/templates/ds/fce/";s:9:"path_page";s:28:"fileadmin/templates/ds/page/";}}';	// Modified or inserted by TYPO3 Extension Manager. 
$TYPO3_CONF_VARS['EXT']['extConf']['realurl'] = 'a:5:{s:10:"configFile";s:26:"typo3conf/realurl_conf.php";s:14:"enableAutoConf";s:1:"1";s:14:"autoConfFormat";s:1:"1";s:12:"enableDevLog";s:1:"0";s:19:"enableChashUrlDebug";s:1:"0";}';	// Modified or inserted by TYPO3 Extension Manager. 
// Updated by TYPO3 Extension Manager 04-07-12 10:09:19
$TYPO3_CONF_VARS['SYS']['sitename'] = 'MEFA Befestigungs- und Montagesysteme GmbH';	// Modified or inserted by TYPO3 Install Tool. 
$TYPO3_CONF_VARS['BE']['disable_exec_function'] = '0';	//  Modified or inserted by TYPO3 Install Tool.
$TYPO3_CONF_VARS['GFX']['gdlib_png'] = '1';	// Modified or inserted by TYPO3 Install Tool. 
$TYPO3_CONF_VARS['GFX']['thumbnails_png'] = '1';	//  Modified or inserted by TYPO3 Install Tool.
$TYPO3_CONF_VARS['GFX']['im_noScaleUp'] = '1';	//  Modified or inserted by TYPO3 Install Tool.
$TYPO3_CONF_VARS['GFX']['jpg_quality'] = '90';	//  Modified or inserted by TYPO3 Install Tool.
$TYPO3_CONF_VARS['BE']['versionNumberInFilename'] = '0';	//  Modified or inserted by TYPO3 Install Tool.
// Updated by TYPO3 Install Tool 04-07-12 10:11:48
// Updated by TYPO3 Extension Manager 04-07-12 10:16:51
// Bo settings
t3lib_extMgm::addUserTSConfig('options.pageTree.showNavTitle = 1');
$TYPO3_CONF_VARS['EXTCONF']['realurl'] = array( '_DEFAULT' => array(
'redirects' => array(), 'preVars' => array(
array(
'GETvar' => 'L',
'valueMap' => array( 'de' => '0',
'en' => '1',
),
'valueDescription' => array(
'de' => 'Deutsch',
'en' => 'English', ),
'noMatch' => 'bypass', ),
),
'pagePath' => array(
'type' => 'user',
'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
'spaceCharacter' => '_', 'languageGetVar' => 'L', 'expireDays' => 7,
),
'fixedPostVars' => array(), 'postVarSets' => array(),
), );
$TYPO3_CONF_VARS['SYS']['compat_version'] = '4.7';
// Updated by TYPO3 Extension Manager 18-02-13 10:37:46
// Updated by TYPO3 Install Tool 18-02-13 13:27:18
// Updated by TYPO3 Extension Manager 08-03-13 10:08:50
$TYPO3_CONF_VARS['EXT']['noEdit'] = '0';	//  Modified or inserted by TYPO3 Install Tool.
// Updated by TYPO3 Install Tool 08-03-13 10:09:49
// Updated by TYPO3 Extension Manager 08-03-13 10:17:14
$TYPO3_CONF_VARS['BE']['installToolPassword'] = '65b787d2c0837b674c4044b48ac2bc4d';	//  Modified or inserted by TYPO3 Install Tool.
// Updated by TYPO3 Install Tool 12-03-13 08:48:50
$TYPO3_CONF_VARS['EXT']['extConf']['languagevisibility'] = 'a:6:{s:18:"inheritanceEnabled";s:1:"0";s:26:"translatedAsDefaultEnabled";s:1:"0";s:8:"useCache";s:1:"0";s:24:"observeCutCopyMoveAction";s:1:"1";s:30:"restrictCutCopyMoveForOverlays";s:1:"0";s:12:"applyPatchTV";s:1:"0";}';	//  Modified or inserted by TYPO3 Extension Manager.
$TYPO3_CONF_VARS['EXT']['extConf']['naw_securedl'] = 'a:15:{s:11:"securedDirs";s:16:"fileadmin/secure";s:8:"filetype";s:58:"pdf|jpe?g|gif|png|odt|ppt?x|doc?x|xls?x|zip|rar|tgz|tar|gz";s:13:"forcedownload";s:1:"1";s:17:"forcedownloadtype";s:44:"odt|ppt?x|doc?x|xls?x|zip|rar|tgz|tar|gz|pdf";s:19:"additionalMimeTypes";s:29:"txt|text/plain,html|text/html";s:6:"domain";s:32:"http://mefa.de/fileadmin/secure/";s:12:"cachetimeadd";s:3:"180";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:9:"524288000";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	// Modified or inserted by TYPO3 Extension Manager.mefa.de/fileadmin/secure/";s:12:"cachetimeadd";s:3:"180";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//mefa.de/fileadmin/secure/";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//mefa.de/";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//mefa.de";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//mefa.de/";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//meda.de/fileadmin/secure/";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//meda.de";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//meda.de";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//meda.de|http://my.other.domain.org";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//meda.de|http://my.other.domain.org";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//meda.de|http://my.other.domain.org";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//meda.de|http://my.other.domain.org";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	//mydomain.com|http://my.other.domain.org";s:12:"cachetimeadd";s:4:"3600";s:5:"debug";s:1:"0";s:3:"log";s:1:"0";s:14:"outputFunction";s:8:"readfile";s:15:"outputChunkSize";s:7:"1048576";s:10:"linkFormat";s:109:"/index.php?eID=tx_nawsecuredl&u=###FEUSER###&g=###FEGROUPS###&t=###TIMEOUT###&hash=###HASH###&file=###FILE###";s:16:"enableGroupCheck";s:1:"0";s:14:"groupCheckDirs";s:0:"";s:13:"excludeGroups";s:0:"";}';	// 
$TYPO3_CONF_VARS['EXT']['extConf']['em'] = 'a:1:{s:17:"selectedLanguages";s:2:"de";}';	//  Modified or inserted by TYPO3 Extension Manager.
// Updated by TYPO3 Extension Manager 24-04-15 09:53:13
?>