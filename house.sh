#!/bin/sh
typo3/cli_dispatch.phpsh lowlevel_cleaner orphan_records -r -v 2 -s --AUTOFIX --YES
typo3/cli_dispatch.phpsh lowlevel_cleaner versions -r -v 2 -s --AUTOFIX --YES
typo3/cli_dispatch.phpsh lowlevel_cleaner tx_templavoila_unusedce -r --refindex update -v 2 -s --AUTOFIX --YES
typo3/cli_dispatch.phpsh lowlevel_cleaner double_files -r --refindex update -v 2 -s --AUTOFIX --YES
typo3/cli_dispatch.phpsh lowlevel_cleaner deleted -r -v 1 -s --AUTOFIX --YES
typo3/cli_dispatch.phpsh lowlevel_cleaner missing_relations -r --refindex update -v 2 -s --AUTOFIX --YES
typo3/cli_dispatch.phpsh lowlevel_cleaner cleanflexform -r -v 2 -s --AUTOFIX --YES
typo3/cli_dispatch.phpsh lowlevel_cleaner rte_images -r --refindex update -v 2 -s --AUTOFIX --YES
typo3/cli_dispatch.phpsh lowlevel_cleaner missing_files -r --refindex update -v 2 -s --AUTOFIX --YES
typo3/cli_dispatch.phpsh lowlevel_cleaner lost_files -r --refindex update -v 2 -s --AUTOFIX --YES